<?php session_start();  
  ini_set("display_errors", E_ALL);
  //Incluir la configuracion
  require_once 'config/config.php';
  //Controlador
  require_once '_controller/ctrl_accesories.php';      
  
  $ctrl = new CtrlAccesories();

  //Incluir vistas

	include_once '_view/header.php';
	include_once '_view/view_accesories.php';
	include_once '_view/footer.php';


