<?php

require_once 'General.php';

class CtrlAnimals extends General {

  public $animals;
  private $searchParameter;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_GET['animal'])){
      if(!empty($_GET['animal'])){
        $this->searchParameter = "'%".$_GET['animal']."%'";
      }else{
        $this->searchParameter = "'%%'";
      }
    }else{
      $this->searchParameter = "'%%'";
    }

    $this->getAnimals($this->searchParameter);
  }

  private function getAnimals($_searchParameter){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_animal,
                  name,
                  description,
                  picture
                  FROM animals
                WHERE name LIKE $_searchParameter order by name;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->execute();
        $res = $cmd->fetchAll(PDO::FETCH_ASSOC);
        $this->animals = json_encode($res);               
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
