<?php

require_once 'General.php';

class CtrlFood extends General {

  public $food;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_GET['id'])){
      if((int)$_GET['id'] != 0){
        $this->getFood($_GET['id']);
      }else{
        die('Intento de contaminar base de datos');
      }
    }else{
      die('No encontramos el animal');
    }
  }

  private function getFood($_idFood){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_food,
                  name,
                  description,
                  picture,
                  price
                FROM foods
                WHERE id_food = :idFood;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->bindParam(':idFood', $_idFood, PDO::PARAM_INT);
        $cmd->execute();
        $res = $cmd->fetchObject();
        if(isset($res->id_food)){
          $this->food = $res;
        }                 
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
