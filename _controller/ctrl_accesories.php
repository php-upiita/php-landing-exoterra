<?php

require_once 'General.php';

class CtrlAccesories extends General {

  public $accesories;
  private $searchParameter;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_GET['accesory'])){
      if(!empty($_GET['accesory'])){
        $this->searchParameter = "'%".$_GET['accesory']."%'";
      }else{
        $this->searchParameter = "'%%'";
      }
    }else{
      $this->searchParameter = "'%%'";
    }

    $this->getFoods($this->searchParameter);
  }

  private function getFoods($_searchParameter){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_accesory,
                  name,
                  description,
                  picture,
                  price
                  FROM accesories
                WHERE name LIKE $_searchParameter order by name;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->execute();
        $res = $cmd->fetchAll(PDO::FETCH_ASSOC);
        $this->accesories = json_encode($res);               
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
