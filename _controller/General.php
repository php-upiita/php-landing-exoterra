<?php
/*
* @author Efra
*/
class General {
  protected $cnxBd = null;
  /*
  * Crea una conexion a la base de datos especificada en 'config'
  * @return boolean
  */
  protected function conectaBd(){
      if($this->cnxBd != null){
          return true;
      }
      else {
          try{
              $this->cnxBd = new PDO('mysql:host='.BD_HOST.';dbname='.BD_NAME.';charset=utf8', BD_USER, BD_PASS);
              //Excepciones SQL
              $this->cnxBd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $this->cnxBd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
              return true;
          }
          catch (PDOException $e) {
              $this->errores[] = "Error en la conexion con la Base de Datos.";
              return false;
          }
      }
  }
}

