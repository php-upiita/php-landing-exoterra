<?php

require_once 'General.php';

class CtrlFoods extends General {

  public $foods;
  private $searchParameter;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_GET['food'])){
      if(!empty($_GET['food'])){
        $this->searchParameter = "'%".$_GET['food']."%'";
      }else{
        $this->searchParameter = "'%%'";
      }
    }else{
      $this->searchParameter = "'%%'";
    }

    $this->getFoods($this->searchParameter);
  }

  private function getFoods($_searchParameter){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_food,
                  name,
                  description,
                  picture,
                  price
                  FROM foods
                WHERE name LIKE $_searchParameter order by name;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->execute();
        $res = $cmd->fetchAll(PDO::FETCH_ASSOC);
        $this->foods = json_encode($res);               
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
