<div class="container">
  <div class="row">
    <div class="col s12 m12 center">
      <h3 class="secundary-color">Tiendas autorizadas</h3>
    </div>
    <div class="col s12 m4 center">
      <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="assets/img/panda.jpg">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Nombre tienda<i class="material-icons right">more_vert</i></span>
          <p><a href="#" class="third-color">Ir al sitio</a></p>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Nombre tienda<i class="material-icons right">close</i></span>
          <p class="left-align" style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo maxime labore suscipit itaque iste fuga facere consequatur voluptatum quis hic, accusantium amet odit ipsam! Necessitatibus, voluptas tempora! Est, sapiente error!</p>
        </div>
      </div>
    </div>
    <div class="col s12 m4 center">
      <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="assets/img/panda.jpg">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Nombre tienda<i class="material-icons right">more_vert</i></span>
          <p><a href="#" class="third-color">Ir al sitio</a></p>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Nombre tienda<i class="material-icons right">close</i></span>
          <p class="left-align" style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo maxime labore suscipit itaque iste fuga facere consequatur voluptatum quis hic, accusantium amet odit ipsam! Necessitatibus, voluptas tempora! Est, sapiente error!</p>
        </div>
      </div>
    </div>
    <div class="col s12 m4 center">
      <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="assets/img/panda.jpg">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Nombre tienda<i class="material-icons right">more_vert</i></span>
          <p><a href="#" class="third-color">Ir al sitio</a></p>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Nombre tienda<i class="material-icons right">close</i></span>
          <p class="left-align" style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo maxime labore suscipit itaque iste fuga facere consequatur voluptatum quis hic, accusantium amet odit ipsam! Necessitatibus, voluptas tempora! Est, sapiente error!</p>
        </div>
      </div>
    </div>
    <div class="col s12 m4 center">
      <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="assets/img/panda.jpg">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4">Nombre tienda<i class="material-icons right">more_vert</i></span>
          <p><a href="#" class="third-color">Ir al sitio</a></p>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">Nombre tienda<i class="material-icons right">close</i></span>
          <p class="left-align" style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo maxime labore suscipit itaque iste fuga facere consequatur voluptatum quis hic, accusantium amet odit ipsam! Necessitatibus, voluptas tempora! Est, sapiente error!</p>
        </div>
      </div>
    </div>
  </div>
</div>


