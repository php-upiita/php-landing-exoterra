<div class="container">
  <div class="row">
    <div class="col s12 m4 center">
      <img src="assets/img/gob_mex.png" style="max-width: 300px; margin-top: 3vh; padding-top: 1.5vh;">
    </div>
    <div class="col s12 m8 center">
      <h3>Requerimientos legales para animáles exóticos</h3>
    </div>
    <div class="col s12 m12 center secundary-color">
      <div class="progress">
        <div class="indeterminate"></div>
      </div>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis vitae quidem facilis, vel ducimus eveniet corporis architecto et ipsum mollitia cum sit cumque quos tenetur commodi ratione nisi, consequatur praesentium.</p>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis vitae quidem facilis, vel ducimus eveniet corporis architecto et ipsum mollitia cum sit cumque quos tenetur commodi ratione nisi, consequatur praesentium.Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis vitae quidem facilis, vel ducimus eveniet corporis architecto et ipsum mollitia cum sit cumque quos tenetur commodi ratione nisi, consequatur praesentium.</p>
      <p class="left-align">1.- Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi cum eos rem molestias? Ducimus impedit qui ab molestiae, aperiam quidem dolores tenetur.</p>
      <p class="left-align">2.- Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi cum eos rem molestias? Ducimus impedit qui ab molestiae, aperiam quidem dolores tenetur.</p>
      <p class="left-align">3.- Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi cum eos rem molestias? Ducimus impedit qui ab molestiae, aperiam quidem dolores tenetur.</p>
      <p class="left-align">4.-Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi cum eos rem molestias? Ducimus impedit qui ab molestiae, aperiam quidem dolores tenetur.</p>
      <p class="left-align">5.- ...</p>
    </div>
  </div>
</div>