<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Exoterra</title>
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="main-nav" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="index.php" class="brand-logo">
      <img src="assets/img/logo.png" class="responsive-img" style="max-width: 140px;">
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="animals.php">Tengo un animal exótico</a></li>
      <li><a href="awareness.php">No tengo un animal exótico</a></li>
      <li><a href="#">Me gustaría informarme</a></li>
    </ul>
    <ul id="nav-mobile" class="sidenav">
      <li><a href="animals.php">Tengo un animal exótico</a></li>
      <li><a href="awareness.php">No tengo un animal exótico</a></li>
      <li><a href="#">Me gustaría informarme</a></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col s12 m12 center">
      <h2 class="header primarycolor">Antes de adquirir recuerda</h2>
    </div>
    <div class="col s12 m6 center">
    <img src="assets/img/fox.jpg" class="responsive-img">
    </div>
    <div class="col s12 m6 left-align" style="text-align:justify;">
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente, blanditiis iusto sint a doloremque sed enim incidunt iste, ipsa veritatis fugiat aperiam laborum similique, at adipisci libero voluptatibus labore harum! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente, blanditiis iusto sint a doloremque sed enim incidunt iste, ipsa veritatis fugiat aperiam laborum similique, at adipisci libero voluptatibus labore harum!</p>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente, blanditiis iusto sint a doloremque sed enim incidunt iste, ipsa veritatis fugiat aperiam laborum similique, at adipisci libero voluptatibus labore harum! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente, blanditiis iusto sint a doloremque sed enim incidunt iste, ipsa veritatis fugiat aperiam laborum similique, at adipisci libero voluptatibus labore harum!</p>

      <div class="row" style="margin-top: 5vh; ">
        <div class="col s6 m6 center">
          <a href="animals.php" class="waves-effect waves-light btn btn-large" style="background-color: #015045;">Ver animales</a>
        </div>
        <div class="col s6 m6 center">
          <a href="stores.php" class="waves-effect waves-light btn btn-large" style="background-color: #015045;">Ver tiendas</a>
        </div>
      </div>
    </div>
  </div>
</div>