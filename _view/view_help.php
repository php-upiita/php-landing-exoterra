<div class="">
  <div class="row">
    <div class="col s12 m12 center secundary-color">
      <h3>¿Cómo puedo ayudar a los animales exóticos?</h3>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m-12">
      <ul class="tabs">
        <li class="tab col s3"><a class="active" href="#test1">Asociasión 1</a></li>
        <li class="tab col s3"><a href="#test2">Asociasión 2</a></li>
        <li class="tab col s3"><a href="#test3">Asociasión 3</a></li>
        <li class="tab col s3"><a href="#test4">Asociasión 4</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12" style="margin-top:5vh;">
      <div class="container center">
      <h4 class="left-align">Título 1</h4>    
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <img src="assets/img/apasdem.png" class="responsive-img" alt="">
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
    </div>
    </div>
    <div id="test2" class="col s12" style="margin-top:5vh;">
      <div class="container center">
      <h4 class="left-align">Título 2</h4>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <img src="assets/img/apasdem.png" class="responsive-img" alt="">
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
    </div>
    </div>
    <div id="test3" class="col s12" style="margin-top:5vh;">
      <div class="container center">
      <h4 class="left-align">Título 3</h4>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <img src="assets/img/apasdem.png" class="responsive-img" alt="">
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
    </div>
    </div>
    <div id="test4" class="col s12" style="margin-top:5vh;">
      <div class="container center">
      <h4 class="left-align">Título 4</h4>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <img src="assets/img/apasdem.png" class="responsive-img" alt="">
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
      <p class="left-align">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem atque voluptate unde reiciendis repellendus porro velit aperiam provident perferendis, ut et laboriosam eum iure sit rerum blanditiis ex temporibus! A?</p>
    </div>
    </div>
  </div>
</div>