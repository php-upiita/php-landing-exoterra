<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Exoterra</title>
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="main-nav" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="index.php" class="brand-logo">
      <img src="assets/img/logo.png" class="responsive-img" style="max-width: 140px;">
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="animals.php">Catálogo de animales</a></li>
      <li><a href="foods.php">Catálogo de alimentos</a></li>
      <li><a href="accesories.php">Catálogo de accesorios</a></li>
      <li><a href="legality.php">Legalidad en México</a></li>
      <li><a href="license.php">Permiso de animales</a></li>
      <li><a href="help.php">Ayuda a otros animales</a></li>
    </ul>
    <ul id="nav-mobile" class="sidenav">
      <li><a href="animals.php">Catálogo de animales</a></li>
      <li><a href="foods.php">Catálogo de alimentos</a></li>
      <li><a href="accesories.php">Catálogo de accesorios</a></li>
      <li><a href="legality.php">Legalidad en México</a></li>
      <li><a href="license.php">Permiso de animales</a></li>
      <li><a href="help.php">Ayuda a otros animales</a></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>