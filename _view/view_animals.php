<div class="container">
  <div class="row">
    <form action="animals.php">
      <div class="col s12 m12">
        <div class="input-field col s12">
          <i class="material-icons prefix">search</i>
          <input type="text" id="autocomplete-input" name="animal" class="autocomplete">
          <label for="autocomplete-input">Animal exótico</label>
        </div>
        <div class="col s12 m12 center">
          <button class="btn waves-effect waves-light" type="submit" name="action">Buscar
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>
  <div class="row">
    <?php 
      $animales = json_decode($ctrl->animals); 
      if(!empty($animales)){
      foreach ($animales as $animal){
    ?>
    <div class="col s12 m4 center">
      <div class="card hoverable">
        <div class="card-image">
          <img src="<?php echo $animalURL.$animal->picture;?>" class="responsive-img">
          <span class="card-title autocomplete"><?php echo $animal->name;?></span>
        </div>
        <div class="card-action">
          <a href="animal.php?id=<?php echo $animal->id_animal;?>">Ver detalle</a>
        </div>
      </div>
    </div>
    <?php } }else{?>
      <div class="col s12 m12 center">
        <h3>No hemos obtenido resultados, por favor realiza de nuevo tu búsqueda</h3>
        <img src="assets/img/error.jpg" class="responsive-img">
      </div>
    <?php } ?>
  </div>
</div>

