<div class="container">
  <div class="row">
    <div class="col s12 m12 left">
      <h2 class="header"><?php echo $ctrl->animal->name;?></h2>
    </div>
    <div class="col s12 m6 left">
      <img src="<?php echo $animalURL.$ctrl->animal->picture; ?>" class="responsive-img materialboxed">
    </div>
    <div class="col s12 m6 left">
      <?php echo $ctrl->animal->description;?>  
      <a class="waves-effect waves-light btn-large" href="<?php echo $animalURL.$ctrl->animal->pdf_link;?>" target="_blank"><i class="material-icons left">picture_as_pdf</i>Ver PDF</a>
    </div>
  </div>
</div>
