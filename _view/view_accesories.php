<div class="container">
  <div class="row">
    <form action="accesories.php">
      <div class="col s12 m12">
        <div class="input-field col s12">
          <i class="material-icons prefix">search</i>
          <input type="text" id="autocomplete-input" name="accesory" class="autocomplete">
          <label for="autocomplete-input">Accesorio exótico</label>
        </div>
        <div class="col s12 m12 center">
          <button class="btn waves-effect waves-light" type="submit" name="action">Buscar
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>
  <div class="row">
    <?php 
      $accesorios = json_decode($ctrl->accesories); 
      if(!empty($accesorios)){
      foreach ($accesorios as $accesorio){
    ?>
    <div class="col s12 m4 center">
      <div class="card hoverable">
        <div class="card-image">
          <img src="<?php echo $accesoryURL.$accesorio->picture;?>" class="responsive-img">
          <span class="card-title autocomplete"><?php echo $accesorio->name;?></span>
        </div>
        <div class="card-content">
          <p class="truncate"><?php echo $accesorio->description;?></p>
        </div>
        <div class="card-action">
          <a href="accesory.php?id=<?php echo $accesorio->id_accesory;?>">Ver detalle</a>
        </div>
      </div>
    </div>
    <?php } }else{?>
      <div class="col s12 m12 center">
        <h3>No hemos obtenido resultados, por favor realiza de nuevo tu búsqueda</h3>
        <img src="assets/img/error.jpg" class="responsive-img">
      </div>
    <?php } ?>
  </div>
</div>

