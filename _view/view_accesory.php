<div class="container">
  <div class="row">
    <div class="col s12 m12 left">
      <h3 class="header"><?php echo $ctrl->accesory->name;?></h3>
    </div>
    <div class="col s12 m6 left">
      <img src="<?php echo $accesoryURL.$ctrl->accesory->picture; ?>" class="responsive-img materialboxed">
    </div>
    <div class="col s12 m6 left">
      <h5>$ <?php echo $ctrl->accesory->price;?></h5>
      <?php echo $ctrl->accesory->description;?>
    </div>
  </div>
</div>
