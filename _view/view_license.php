<div class="container">
  <div class="row">
    <div class="col s12 m12 center">
      <h3 class="secundary-color">Guía para obtener el permiso de animales exótico en México.</h3>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12 left-align">
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque aperiam, veniam quibusdam, tempore quas sit eveniet rem, rerum dolore iste omnis ullam atque deleniti molestias assumenda magnam quidem eaque deserunt! Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis, libero? Possimus, architecto voluptatibus commodi ducimus dolores cumque nesciunt voluptatem ullam culpa necessitatibus veniam. Eum voluptatibus cum adipisci facere explicabo. Ipsa.</p>
      <p><b class="third-color">Paso 1:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 2:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 3:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 4:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 5:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 6:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 7:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
      <p><b class="third-color">Paso 8:</b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum, error accusantium. Enim aliquam eos officiis sunt, aspernatur delectus quaerat placeat esse, error, porro aliquid eum perferendis maxime voluptate ab unde.</p>
    </div>
  </div>
</div>


