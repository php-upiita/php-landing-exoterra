    <footer class="page-footer main-footer">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Nosotros</h5>
            <p class="grey-text text-lighten-4">
              Somos una empresa Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse, nihil, ullam quos excepturi sint a porro accusamus delectus accusantium at aliquid. Eos animi consequatur nam suscipit fugiat, provident vero! Fugit?
            </p>
          </div>
          <div class="col l3 s12">
            <h5 class="white-text">Tipo de usuario</h5>
            <ul>
              <li><a class="white-text" href="animals.php">Tengo animal exótico</a></li>
              <li><a class="white-text" href="awareness.php">No Tengo animal exótico</a></li>
              <li><a class="white-text" href="animals.php">Me gustaría informarme</a></li>
            </ul>
          </div>
          <div class="col l3 s12">
            <h5 class="white-text">Contáctanos</h5>
            <ul>
              <li><a class="white-text" href="#!">+52 55 5555 5555</a></li>
              <li><a class="white-text" href="#!">contacto@exoterra.com</a></li>

            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        Made by <a class="brown-text text-lighten-3" href="https://gitlab.com/efrabaez" target="_blank">Efra's Development</a>
        </div>
      </div>
    </footer>
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="assets/js/materialize.js"></script>
    <script src="assets/js/init.js"></script>
    <script>
      $(document).ready(function(){
        $('.materialboxed').materialbox();
      });

      $(document).ready(function(){
        $('.tabs').tabs();
      });

      $('.carousel.carousel-slider').carousel({
        fullWidth: true
      });
    </script>
  </body>
</html>
