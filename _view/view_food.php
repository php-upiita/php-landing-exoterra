<div class="container">
  <div class="row">
    <div class="col s12 m12 left">
      <h3 class="header"><?php echo $ctrl->food->name;?></h3>
    </div>
    <div class="col s12 m6 left">
      <img src="<?php echo $foodURL.$ctrl->food->picture; ?>" class="responsive-img materialboxed">
    </div>
    <div class="col s12 m6 left">
      <h5>$ <?php echo $ctrl->food->price;?></h5>
      <?php echo $ctrl->food->description;?>
    </div>
  </div>
</div>
