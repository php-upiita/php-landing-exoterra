<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Exoterra</title>
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="main-nav" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="index.php" class="brand-logo">
      <img src="assets/img/logo.png" class="responsive-img" style="max-width: 140px;">
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="animals.php">Tengo un animal exótico</a></li>
      <li><a href="awareness.php">No tengo un animal exótico</a></li>
      <li><a href="animals.php">Me gustaría informarme</a></li>
    </ul>
    <ul id="nav-mobile" class="sidenav">
      <li><a href="animals.php">Tengo un animal exótico</a></li>
      <li><a href="awareness.php">No tengo un animal exótico</a></li>
      <li><a href="animals.php">Me gustaría informarme</a></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>

<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m6 offset-m3">
        <h3 class="center primary-color">Tipos de usuario</h3>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m4 center">
        <div class="icon-block">
          <a href="animals.php"><img src="assets/img/icons/icon-exotics.png" class="responsive-img user-img"></a>
          <h5 class="center">Tengo un animal exótico</h5>
        </div>
      </div>
      <div class="col s12 m4 center">
        <div class="icon-block">
        <a href="awareness.php"><img src="assets/img/icons/icon-exotics.png" class="responsive-img user-img user-img-greyscale"></a>
          <h5 class="center">No tengo un animal exótico</h5>
        </div>
      </div>
      <div class="col s12 m4 center">
        <div class="icon-block">
        <a href="animals.php"><img src="assets/img/icons/icon-exotics-info.png" class="responsive-img user-img"></a>
          <h5 class="center">Me gustaría informarme</h5>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="parallax-container valign-wrapper">
  <div class="section no-pad-bot">
    <div class="container">
      <div class="row center">
        <h5 class="header col s12 light">Lorem ipsum dolor sit amet consectetur adipisicing elit</h5>
      </div>
    </div>
  </div>
  <div class="parallax"><img src="assets/img/hero.jpg" alt="Unsplashed background img 2"></div>
</div>


<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m12">
        <h5 class="left-align">Tengo un animal exótico</h5>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At, dicta ipsam? Numquam incidunt excepturi itaque nemo eius, provident possimus modi! Distinctio placeat dolores autem aut dolor provident delectus debitis voluptas.</p>
      </div>
      <div class="col s12 m12">
        <h5 class="left-align">No tengo un animal exótico</h5>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At, dicta ipsam? Numquam incidunt excepturi itaque nemo eius, provident possimus modi! Distinctio placeat dolores autem aut dolor provident delectus debitis voluptas.</p>
      </div>
      <div class="col s12 m12">
        <h5 class="left-align">Me gustaría conocer del tema</h5>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. At, dicta ipsam? Numquam incidunt excepturi itaque nemo eius, provident possimus modi! Distinctio placeat dolores autem aut dolor provident delectus debitis voluptas.</p>
      </div>
    </div>
    <div class="row">
      <div class="col s4 m4 center">
        <div class="icon-block">
          <a href="animals.php"><img src="assets/img/icons/icon-exotics.png" class="responsive-img"></a>
        </div>
      </div>
      <div class="col s4 m4 center">
        <div class="icon-block">
        <a href="awareness.php"><img src="assets/img/icons/icon-exotics.png" class="responsive-img user-img-greyscale"></a>
        </div>
      </div>
      <div class="col s4 m4 center">
        <div class="icon-block">
        <a href="animals.php"><img src="assets/img/icons/icon-exotics-info.png" class="responsive-img"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 center">
        <h5>Elige tu tipo de usuario</h5>
      </div>
    </div>
  </div>
</div>